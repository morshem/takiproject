#pragma once 
#pragma comment(lib, "sqlite3.lib")
#pragma comment(lib, "Ws2_32.lib")

enum colors_enum
{
	red,
	green,
	yellow,
	blue,
	none
};

//1-9:values as the number
//special cards values starts from 50-55 and super taki has special value-69
enum card_values_enum
{
	one=1,
	two,
	three,
	four,
	five,
	six,
	seven,
	eight,
	nine,
	plus=50,
	stop,
	change_color, 
	taki,
	super_taki=69
};

enum errors_enum
{
	success,
	invalid_socket,
	sending_data_err,
	listening_error,
	intiate_server_err,
	binding_sock_err,
	invalid_turn
};