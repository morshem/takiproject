#pragma once
#include "User.h"
#include "Room.h"
class PlayingUser :
	public User
{
public:
	PlayingUser(SOCKET sock, string name, bool isAdmin, Room *room, Card cards[START_CARDS_IN_HAND]);
	~PlayingUser();
	errors_enum placeCard(vector<Card> played);
private:
	Card _cardsInHand[8];
	bool _isAdmin;
	Room *_room;
};

