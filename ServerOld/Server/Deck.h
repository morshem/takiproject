#pragma once
#include "Card.h"
#include <algorithm>
#include <chrono> 
#include <random>
#include <vector>

using namespace std;
class Deck
{
public:
	Deck();
	~Deck();
	Card Draw();//Draw 1 card from the deck
	void ShuffleDeck();//shuffle the deck
	vector<Card> StartingGameDraw();
private :
	Card _deck[112];
	int _last_picked;
	
};

