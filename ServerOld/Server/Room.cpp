#include "Room.h"
/**/

Room::Room(User *admin, string room_name)
{
	_admin = admin;
	_players = (User*)(malloc(sizeof(User) * 4));
	_room_name = room_name;
	_draw_counter = 1;
}


Room::~Room()
{
	for (int i = 0; i < _current_player; i++)
	{
		delete (_players + i);
	}
	delete _admin;

}


bool Room::AddUser(User &user){
	//����� : ����� ����� ������� ����
	//����� : ������ ���� ����, ������ ������
	if (_current_player < 4)
	{
		_players[_current_player] = user;
		return true;
	}
	return false;
	
}


void Room::DeleteUser(const User &user){
	//����� : ����� ����� ����� �����
	//����� : ������ ���� �����
	for (int i = 0; i < _current_player; i++)
	{
		if (_players[i] == user)
		{

			for (int j = i; j <_current_player-1; j++)
			{
				_players[j] = _players[j+1];
			}
		}
	}
	_current_player--;
}


const bool Room::is_open(){
	//����� : ���
	//����� : ��� ����� �����
	return this->_in_game;
}


void Room::close(){
	//����� : ���
	//����� : ����� �����
	_in_game = false;
}

const bool Room::is_in_room(const User  &user){
	//����� : ������ ����� ����� ���� ��� ��� ���� ����
	//����� : ��� ������ ���� ����
	for (int i = 0; i < _current_player; i++)
	{
		if (_players[i] == user)
			return true;
	}
	return ((*_admin )== user);
}


void Room::start_game(){
	//����� : ���
	//����� : ����� �����, ������ ������
	_in_game = true;
}


bool Room::play_turn(const vector<Card> moves){
	//����� : ���� �� ������ ���� ���
	//����� : ����� ������ �� ����� ����� �� �����, ����� ������
	if (!is_turn_legal(moves))
		return false;
	_last_card = new Card( moves[moves.size() - 1]);
}





bool Room::is_turn_legal(vector<Card> moves)
{
	//����� : ���� �� �����
	//����� : ��� ���� ����
	Card *last = _last_card;
	for (vector<Card>::iterator i = moves.begin(); i !=moves.end(); i++)
	{
		if (!last->isValid(*i))
			return false;
		last = &(*i);
	}
	return true;



}
const bool Room::is_draw_legal(int num_of_cards){
	//����� ��� ������ ����� ��� ���� ����.
	if (_last_card->value() != card_values_enum::two&&num_of_cards > 1)
		return false;
	return true;

}

vector<Card> Room::shuffle_cards(const int num_of_cards){
	//����� : ���� ����� �����.
	//����� : ���� �� ������ ������
	_deck.ShuffleDeck();
	vector<Card> cards;
	for (int i = 0; i < num_of_cards; i++)
	{
		cards.push_back(_deck.Draw());
	}
	return cards;
}

vector<vector<Card> > Room::shuffle_cards_start_game(const int num_of_players){
	//����� : ���� ������
	//����� : ���� �� �����, ��� ���� ��� ���� �� ���� ������ ��� ������ �����
	vector<vector<Card>> players;
//	Card *ptr;
	for (int i = 0; i < num_of_players; i++)
	{
		players[i].push_back(_deck.Draw());
	}
	return players;

}

bool Room::Chat(string msg)
{
	bool flag = true;
	for (int i = 0; i < this->_current_player; i++)
	{
		flag = flag && (_players[i].sendUpdate(msg) == errors_enum::success);
	}
	return flag;
}
