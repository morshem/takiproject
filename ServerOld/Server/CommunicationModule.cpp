#include "CommHandler.h"
#include <iostream>
#include <thread>

void main()
{
	CommHandler CommunicationModule;
	SOCKET serverSock;
	sockaddr_in clientAddr, serverAddr;

	CommunicationModule.InitSock(serverSock);
	CommunicationModule.initServer(clientAddr, 8888);// Test port

	serverAddr.sin_addr.s_addr = AF_INET;
	serverAddr.sin_family = inet_addr("10.2.0.46");
	serverAddr.sin_port = htons(8888);

	CommunicationModule.bind_sock(serverSock, (const sockaddr*)&serverAddr);
	CommunicationModule.Listen(serverSock);


}


void 