#include "CommHandler.h"
/**/
//Initiate a socket
errors_enum CommHandler::InitSock(SOCKET &sock)
{
	sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sock == INVALID_SOCKET)
	{
		return errors_enum::invalid_socket;
	}
	else
	{
		return errors_enum::success;
	}
}


//send data to the client
errors_enum CommHandler::SendData(SOCKET &sock, string data)
{
	try
	{
		if (send(sock, data.c_str(), data.length(), 0) == SOCKET_ERROR)
			return errors_enum::sending_data_err;
	}
	catch (exception ex)
	{
		return errors_enum::sending_data_err;
	}
	return errors_enum::success;
}



//Recive data. if no data recived - return null
void CommHandler::ReciveData(SOCKET sock)
{
	char *ch = new char(100);
	try
	{
		if (recv(sock, ch, 100, 0) == SOCKET_ERROR)
			cout << "";
		//	return nullptr;
	}
	catch (exception ex)
	{
		//return nullptr;
	}
	//return string(ch);
}



//set the socket into a listening mode
errors_enum CommHandler::Listen(SOCKET sock)
{
	try
	{
		if (listen(sock, SOMAXCONN) == SOCKET_ERROR)
			return errors_enum::listening_error;
	}
	catch (exception ex)
	{
		return errors_enum::listening_error;
	}
	return errors_enum::success;
}

//bind a socket to the listen socket
errors_enum CommHandler::bind_sock(SOCKET listenSocket, const struct sockaddr *name)
{
	if (bind(listenSocket, name, sizeof(*name)) == SOCKET_ERROR)
	{
		cout << "Error binding ! code: " << WSAGetLastError();
		return errors_enum::binding_sock_err;
	}
	return errors_enum::success;
}

//Initiate a server
errors_enum CommHandler::initServer(sockaddr_in &client, int port)
{
	try
	{
		client.sin_family = AF_INET;
		client.sin_addr.s_addr = INADDR_ANY;
		client.sin_port = htons(port);
	}
	catch (exception ex)
	{
		return errors_enum::intiate_server_err;
	}
	return errors_enum::success;
}
