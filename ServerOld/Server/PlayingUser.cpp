#include "PlayingUser.h"

/**/
PlayingUser::PlayingUser(SOCKET sock, string name, bool isAdmin, Room *room, Card cards[START_CARDS_IN_HAND]) :User(sock,name)
{
	_isAdmin = isAdmin;
	_room = room;
	for (int i = 0; i < START_CARDS_IN_HAND; i++)
	{
		_cardsInHand[i] = cards[i];
	}
}


PlayingUser::~PlayingUser()
{
}
errors_enum PlayingUser::placeCard(vector<Card> played)
{
	if (!_room->is_turn_legal(played))
		return errors_enum::invalid_turn;
	return errors_enum::success;
}