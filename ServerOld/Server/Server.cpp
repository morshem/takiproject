#include <WinSock2.h>

#include "Server.h"
/**/
Server::Server()
{

	
}

void Server::Run(int port)
{
	WSADATA info;
	int err,
		connections = 0;
	SOCKET listenSocket, acceptSocket;
	CommHandler::InitSock(listenSocket);
	sockaddr_in server;
	CommHandler::initServer(server, port);
	err = WSAStartup(MAKEWORD(2, 0), &info);
	if (err != 0)
	{
		cout << "WSAStartup failed code %d\n" << err;;
	}

	CommHandler::bind_sock(listenSocket, (sockaddr*)&server);
	CommHandler::Listen(listenSocket);
	cout << "Waiting for client to connect...\n";
	while (true)
	{
		acceptSocket = accept(listenSocket, NULL, NULL);
		if (acceptSocket == INVALID_SOCKET)
		{
			cout << "accept function failed with error: \n" << WSAGetLastError();
			continue;
		}

		acceptSocket = accept(listenSocket, NULL, NULL);
		cout << "Client connected.\n";
		std::thread(CommHandler::ReciveData, listenSocket);

	}

	closesocket(acceptSocket);
	closesocket(listenSocket);
}
