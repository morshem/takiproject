#pragma once
#include "Sqlite/sqlite3.h"
#include "Sqlite\sqlite3.h"
#include <iostream>
#include "User.h"
#include <string>
using namespace std;
class DatabaseHandler
{
public :

	
private:
	static bool opendb(sqlite3 **db);
	static bool preparedb(sqlite3 **db, string sql, sqlite3_stmt **stmt);
	static void exec_query(sqlite3 *db, string sql, sqlite3_stmt *stmt);
	static User* GetUser(string username, string pass);
};

