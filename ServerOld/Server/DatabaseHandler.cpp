#include "DatabaseHandler.h"


bool DatabaseHandler::opendb(sqlite3 **db)
{

	if (sqlite3_open("AppData/db", db) != SQLITE_OK)
	{
		cout << "Failed to open database! error" << sqlite3_errmsg(*db);
		sqlite3_close(*db);
		return false;
	}
	
	return true;
}


bool DatabaseHandler::preparedb(sqlite3 **db, string query, sqlite3_stmt **stmt)
{
	//prepare the sql, leave stmt ready for loop 

	if (sqlite3_prepare_v2(*db, query.c_str(), query.size() + 1, stmt, NULL) != SQLITE_OK) {
		printf("Failed to prepare database %s\n\r", sqlite3_errmsg(*db));
		sqlite3_close(*db);
		return false;
	}
	return true;

}



User* DatabaseHandler::GetUser(string username,string pass)
{
	sqlite3 *db;
	sqlite3_stmt *stmt;
	string query,
		password;
	query = "SELECT password FROM Users Where UserName='" + username + "'";
	if (!opendb(&db))
		return nullptr;
	if (!preparedb(&db, query, &stmt))
		return nullptr;
	if (sqlite3_step(stmt) != SQLITE_ROW)
		return nullptr;
	password=string ((char*)sqlite3_column_text(stmt, 0));
	if (password != pass)
	{
		cout << "Login failed ! wrong password!"<<endl;
		return nullptr;
	}
	sqlite3_close(db);
	return new User(username, password);


}