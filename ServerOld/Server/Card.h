#pragma once
#define START_CARDS_IN_HAND 8
#include "enums.h"
class Card
{
public:
	Card();
	const enum card_values_enum value();
	Card(enum colors_enum _color,enum card_values_enum _value);
	//Card(const Card &c);
	void operator=(const Card &copied);
	bool isValid(Card c);
	const enum colors_enum color();
	bool operator==(Card &c);
private:
	enum colors_enum _color;
	enum card_values_enum _value;
};

