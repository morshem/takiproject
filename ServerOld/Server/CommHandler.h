#pragma once
#include <WinSock2.h>
#include <iostream>
#include "enums.h"

using namespace std;
 class CommHandler
{
public :
	//Initiate a socket
	static errors_enum InitSock(SOCKET &sock);
	//Initiate a server
	static errors_enum initServer(sockaddr_in &client, int port);
	//send data
	static errors_enum SendData(SOCKET &sock, string data);
	//Recive data. if no data recived - return null
	static void ReciveData(SOCKET sock);
	//set the socket into a listening mode
	static errors_enum Listen(SOCKET sock);
	//bint a socket to the listen socket
	static errors_enum bind_sock(SOCKET listenSocket, const struct sockaddr *name);
};

