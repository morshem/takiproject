#include "Deck.h"
/**/
Deck::Deck()
{
	colors_enum color;
	for (int k = 0; k <2; k++)
	{
		for (int i = 0; i < 4; i++)
		{
			color = static_cast<colors_enum>(i);
			for (int j = 0; j < 9; j++)
			{

				_deck[(i * 12 +k*52) + j] = Card(color, static_cast<card_values_enum>(j));
			}
			_deck[9 + i * 12 + k * 52] = Card(color, card_values_enum::taki);
			_deck[10 + i * 12 + k * 52] = Card(color, card_values_enum::stop);
			_deck[11 + i * 12 + k * 52] = Card(color, card_values_enum::plus);
		}
		_deck[48 + k * 52] = Card(colors_enum::none, card_values_enum::super_taki);
		_deck[49 + k * 52] = Card(colors_enum::none, card_values_enum::super_taki);
		_deck[50 + k * 52] = Card(colors_enum::none, card_values_enum::change_color);
		_deck[51 + k * 52] = Card(colors_enum::none, card_values_enum::change_color);
	}
	this->ShuffleDeck();
	_last_picked = 0;
}

Deck::~Deck()
{
	for (int i = 0; i < 112; i++)
	{
		delete (_deck+i);
	}
}

void Deck::ShuffleDeck()
{
	unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();// obtain a time based seed
	std::shuffle(std::begin(_deck), std::end(_deck),std::default_random_engine( seed));//shuffle the deck
}
Card Deck::Draw()
{
	return _deck[_last_picked++];
}

vector<Card> Deck::StartingGameDraw()
{
	vector<Card> cards;
	for (int i = 0; i < START_CARDS_IN_HAND; i++)
	{
		cards.push_back(Draw());
	}
	return cards;
}

