#pragma once
#include "CommHandler.h"
#include "Card.h"
class User
{
public:
	User(SOCKET sock,string name);
	User(string name, string password);
	~User();
	
	virtual errors_enum sendUpdate(string update);
	const bool operator==(const User &u);
	const SOCKET get_sock() const;
private:
	SOCKET _userSock;

	string _name;
	string _pass;
};

