#pragma once
#include "User.h"
#include <vector>
#include <random>
#include "Deck.h"
class Room
{
public:
	Room(User *admin,string room_name);

	~Room();

	//����� ����� ��� ������ ����
	bool Chat(string msg);
	bool AddUser(User &user);
	//����� : ����� ����� ������� ����
	//����� : ������ ���� ����, ������ ������


	void DeleteUser(const User &user);
	//����� : ����� ����� ����� �����
	//����� : ������ ���� �����


	const bool is_open();
	//����� : ���
	//����� : ��� ����� �����


	void close();
	//����� : ���
	//����� : ����� �����


	const bool is_in_room(const User  &user);
	//����� : ������ ����� ����� ���� ��� ��� ���� ����
	//����� : ��� ������ ���� ����



	void start_game();
	//����� : ���
	//����� : ����� �����, ������ ������



	bool play_turn(const vector<Card> moves);
	//����� : ���� �� ������ ���� ���
	//����� : ����� ������ �� ����� ����� �� �����, ����� ������




	bool draw_cards(const int card_number);
	//����� : ���� ������ ������� ������.
	//����� : ����� ������ �� ����� ����� �� �����, ����� ������



	bool is_turn_legal(const vector<Card> moves);const
	//����� : ���� �� �����
	//����� : ��� ���� ����




	bool is_draw_legal(int num_of_cards);
	//����� ��� ������ ����� ��� ���� ����.



	vector<Card> shuffle_cards(const int num_of_cards);
	//����� : ���� ����� �����.
	//����� : ���� �� ������ ������



	vector<vector<Card> > shuffle_cards_start_game(const int num_of_players);
	//����� : ���� ������
	//����� : ���� �� �����, ��� ���� ��� ���� �� ���� ������ ��� ������ �����

private:
	string _room_name;
	User *_admin,
		*_players;
	bool _in_game;
	int _current_player,
		_turn_modifier,
		_draw_counter;
	Card *_last_card;
	Deck _deck;
};

