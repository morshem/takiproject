#include "User.h"

/**/
User::User(SOCKET sock, string name)
{
	this->_userSock = sock;
	this->_name = name;
}

User::User(string name, string password)
{
	_pass = password;
	_name = name;
	CommHandler::InitSock(_userSock);
}

const bool User::operator==(const User& other)
{
	if (this->get_sock() == other.get_sock())
	{
		return true;
	}
	return false;
}

User::~User()
{
	closesocket(_userSock);
	
}
errors_enum User::sendUpdate(string update)
{
	return CommHandler::SendData(_userSock, update);

}

const SOCKET User::get_sock() const
{
	return this->_userSock;
}