import socket
import sys

#create an INET, STREAMing socket
serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#bind the socket to a public host,
# and a well-known port
hostName = socket.gethostname()
print hostName
print socket.gethostbyname(socket.gethostname())
serversocket.bind((socket.gethostbyname(hostName), 8888))
#become a server socket
serversocket.listen(4)
(clientsocket, address) = serversocket.accept()
print "connected"

while True:
    print clientsocket.recv(1024)