#ifndef ROOM_H
#define ROOM_H
#include <vector>
#include <thread>
# include<WinSock2.h>
# include<Windows.h>
#include <string>
#include <iostream>
#include "Card.h"
#include "User.h"
using namespace std;
class Room
{
private:
	bool taki;
	string roomname;
	vector<User*> users;//first is admin
	//vector<Card> cards;
	bool isInGame;
	Card _topCard;
	int currTurn;
	bool rotate;
	string lastComment;
public:
	int drawCount;
	bool activeplus2;

	void endtaki(User user, string card);
	bool getRotate();
	void switchRotate();
	int getTurn();
	void attemptPlay(User user,string cards);
	void advanceTurn(int num);
	Card getCard();
	void setCard(Card card);
	bool GameState();
	void RunGame();
	bool isPremium;
	bool add_user(User* user);
	int getSize();
	vector<User*> getUsers();
	User* getAdmin();
	void add_user_lob(User* user);
	void delete_user(User &user);
	string getName();
	void sendCards();
	int is_turn_legal(Card _move);
	Room(string name, bool premium);
	~Room();
};
#endif
