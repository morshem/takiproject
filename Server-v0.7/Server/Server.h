#pragma once
#include <vector>
#include <thread>
#include<WinSock2.h>
#include<Windows.h>
#include "Msg.h"
#include <exception>
#include <iostream>
#include <string>
#include "Room.h"
#include "sqlite3.h"

//The main class of the project
class Server
{
public:
	Server(int port);
	~Server();


	bool Listen();
	std::thread* startAccept();
	bool stopListen();
	void handleClient(User* curruser);
	int maxrooms;
	std::vector<Room> rooms;
	void cleanEmptyRoom();
	void sql_select(const char* sql);
	Room* getRoomIn(string name);
	string sqlAns;
	sqlite3 *db;


private:
	int _port;
	int _socket;
	WSADATA _info;
	sockaddr_in _addi;
	
};