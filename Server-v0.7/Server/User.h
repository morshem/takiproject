#ifndef USER_HEADER
#define USER_HEADER

#include <vector>
#include <thread>
# include<WinSock2.h>
# include<Windows.h>
#include <string>
#include <iostream>
#include "Card.h"
using std::string;

class Room;


class User
{
private:
	std::string username;
	SOCKET soc;
	string roomname;
	bool verified;
	std::vector <Card> cards;

public:
	void pushCard(Card card);
	void deleteCard(Card card);
	void setRoom(string room);
	std::vector <Card> getCards();
	string getRoom();
	User(std::string name, SOCKET soc);
	User();
	std::string getname();
	SOCKET getsoc();
	bool operator==(User otheru);
	void login(std::string name);
	bool isLoggedIn();
	~User();
};

#endif
