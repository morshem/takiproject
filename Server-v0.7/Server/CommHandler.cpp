#include "CommHandler.h"


CommHandler::CommHandler()
{

}

CommHandler::~CommHandler()
{

}

void CommHandler::handleMsg(User* user, string msg, Server* serv)
{
	int code;
	string args = "";
	int i;
	int temp1;

	string code_str = "";

	for (int i = 1; i < 3 /*Only until the last possible index of a code part*/ ; i++)//get the code number
	{
		code_str += msg[i];
	}
	code = atoi(code_str.c_str());

	for (int i = 0; i < msg.length(); i++)
	{
		if (msg[i] == '|')
		{
			i++;
			for (int j = i; j < msg.find("||"); j++)
			{
				args += msg[j];
			}
			break;
		}
	}
	;
	switch (code)
	{
		case 1://register
			attemptRegister(user, args, serv);
			break;
		case 2: //login
			attemptLogin(user, args, serv);
			break;
		
		case 10: //send roomlist
			sendRoomList(user, serv);
			break;
		case 11:
			createNewRoom(user, args, serv);
			break;
		case 12: //change user room
			addToRoom(user, args, serv);
			break;
		case 13: //start game
			if (*(getRoomIn(user->getRoom(), serv)->getAdmin()) == *user)
			{
				if (getRoomIn(user->getRoom(), serv)->getSize() >= 2)
				{
					cout << "started game" << endl;
					startGame(user, serv);
				}
			}
			break;
		case 20: //play cards
			getRoomIn(user->getRoom(), serv)->attemptPlay(*user, args);
			syncGame(user, serv);
			break;
		case 21: //draw cards
			drawCard(user, serv);
			break;
		
		
		case 30://Msg Sent.
		{
			Msg newmsg(*user, args);
			sendMsg(newmsg, serv);
			break;
		}
		case 111:
			sendUserList(*user, serv);
			break;
		case 999:
			for (int i = 0; i < serv->getRoomIn(user->getRoom())->getUsers().size(); i++)
			{
				send(serv->getRoomIn(user->getRoom())->getUsers()[i]->getsoc(), "@999||", 7, 0);
			}
		default:
			break;
	}
}

void CommHandler::sendMsg(Msg msgSent,Server* serv)
{
	int temp1;
	cout << serv->getRoomIn(msgSent.getUser().getRoom())->getName() << "-" << serv->getRoomIn(msgSent.getUser().getRoom())->getSize();
	string temp = "@300|" + msgSent.getMsg() + "||";
	vector<User*> users = serv->getRoomIn(msgSent.getUser().getRoom())->getUsers();
	for (int i = 0; i < serv->getRoomIn(msgSent.getUser().getRoom())->getSize(); i++)
	{
		send(users[i]->getsoc(), (temp).c_str(), strlen(temp.c_str()) + 1, 0);
	}
	
}

void CommHandler::attemptLogin(User* user, string name_pass, Server* serv)
{
	string name;
	string pass;
	int i;
	for (i = 0; i < name_pass.find("|"); i++)
	{
		name += name_pass[i];
	}
	name_pass.erase(name_pass.begin(), name_pass.begin() + i + 1);
	pass = name_pass;
	if (pass.find("'") != string::npos || name.find("'") != string::npos)
	{
		string ans = "@120| ||";
		cout << user->getsoc() << " failed to log in with the username " << name << " cuz of SQL injection" << endl;
		send(user->getsoc(), ans.c_str(), ans.size() + 1, 0);
		return;
	}
	serv->sql_select(("SELECT username FROM Users WHERE username='" + name + "' AND password='" + pass + "'").c_str());
	if (serv->sqlAns != "")
	{
		user->login(name);
		string ans = "@100|"+name+"||";
		send(user->getsoc(), ans.c_str(), ans.size() + 1, 0);
		sendRoomList(user,serv);
	}
	else
	{
		string ans = "@120| ||";
		cout << user->getsoc() << " failed to log in with the username " << name << endl;
		send(user->getsoc(), ans.c_str(), ans.size() + 1, 0);
	}
	return;
}

void CommHandler::sendRoomList(User* user, Server* serv)
{
	string list = "@114|";
	for (int i = 0; i < serv->rooms.size(); i++)
		list += serv->rooms[i].getName() + "|";
	list += "|";
	send(user->getsoc(), list.c_str(), list.size() + 1, 0);
}

bool CommHandler::addToRoom(User* user, string name, Server* serv)
{
	Room* temp = serv->getRoomIn(user->getRoom());
	int i;
	for (i = 0; i < serv->rooms.size(); i++)
	{
		if (serv->rooms[i].getName() == name)
		{
			if (serv->rooms[i].add_user(user))
			{
				temp->delete_user(*user);
				user->setRoom(serv->rooms[i].getName());
				sendUserList(*user,serv);
				cout << user->getname() << " entered " << user->getRoom() << endl;
				return true;
			}
			//failure
			string fullRoomMsg = "@123|" + name + "||";
			send(user->getsoc(), (fullRoomMsg).c_str(), fullRoomMsg.size() + 1, 0);
			cout << user->getname() << " failed to enter" << user->getRoom() << endl;
			return false;
		}
	}
	string noRoomMsg = "@124|" + name + "||";
	send(user->getsoc(), (noRoomMsg).c_str(), noRoomMsg.size() + 1, 0);
	cout << user->getname() << "tried enter a non existing room " << name << endl;
	return false;
}

void CommHandler::attemptRegister(User* user, string name_pass, Server* serv)
{
	string name;
	string pass;
	int i;
	cout << name_pass << endl;
	cout << name_pass.find("|") << endl;
	for (i = 0; i < name_pass.find("|"); i++)
	{
		name += name_pass[i];
	}
	name_pass.erase(name_pass.begin(), name_pass.begin() + i + 1);
	pass = name_pass;
	serv->sql_select(("SELECT username FROM Users WHERE username='" + name + "'").c_str());
	if (serv->sqlAns == "")
	{
		serv->sql_select(("INSERT INTO Users(username,password) VALUES('" + name + "','" + pass + "');").c_str());
		cout << user->getsoc() << " registered with the username " << name << endl;
		user->login(name);
		string ans = "@101| ||";
		send(user->getsoc(), ans.c_str(), ans.size() + 1, 0);
		sendRoomList(user, serv);
	}
	else
	{
		string ans = "@122| ||";
		cout << user->getsoc() << " failed to register in with the username " << name << endl;
		send(user->getsoc(), ans.c_str(), ans.size() + 1, 0);
	}
	return;
}

void CommHandler::createNewRoom(User* user, string name, Server* serv)
{
	if (user->isLoggedIn() != true || serv->rooms.size() >= serv->maxrooms)
	{
		cout << user->getname() << " failed to creat a room named " << name << endl;
		string ans = "@102|INVALID||";
		send(user->getsoc(), ans.c_str(), ans.size() + 1, 0);
		return;
	}
	cout << user->getname() << " created a room named " << name << endl;
	Room room(name, true);
	serv->rooms.push_back(room);
	string ans = "@102|" + name + "||";
	send(user->getsoc(), ans.c_str(), ans.size() + 1, 0);
	sendRoomList(user, serv);
	return;
}

void CommHandler::sendUserList(User user, Server* serv)
{
	string userlist = "@103|";
	Room* room;
	for (int i = 0; i < serv->getRoomIn(user.getRoom())->getUsers().size(); i++)
	{
		room = serv->getRoomIn(user.getRoom());
		userlist += (room->getUsers())[i]->getname() + "|";
	}
	if (serv->getRoomIn(user.getRoom())->getUsers().size() == 0)
		userlist += "|";
	userlist += "|";
	send(user.getsoc(), (userlist).c_str(), userlist.size() + 1, 0);
}

Room* CommHandler::getRoomIn(string name, Server* serv)
{
	for (int i = 0; i < serv->rooms.size(); i++)
	{
		if (serv->rooms[i].getName() == name)
			return &(serv->rooms[i]);
	}
	return nullptr;
}

/*
112-startgame
112-cards
210-num of cards of players
*/
void CommHandler::startGame(User* user, Server* serv)
{
	cout << user->getname() <<" started game" << endl;
	Room* curr = getRoomIn(user->getRoom(), serv);
	curr->RunGame();
	for (int i = 0; i < curr->getSize(); i++)
	{
		for (int j = 0; j < 8; j++)
		{
			curr->getUsers()[i]->pushCard(getRandomCard());
		}
	}
	curr->setCard(getRandomCard());

	string msg = "";
	for (int i = 0; i < curr->getSize(); i++)
	{
		msg = "@112|";
		for (int j = 0; j < 8; j++)
			msg += curr->getUsers()[i]->getCards()[j].getype() + ",";
		msg.erase(msg.size() - 1);
		msg += "|" + curr->getCard().getype();
		msg += "||";
		send(curr->getUsers()[i]->getsoc(), (msg).c_str(), msg.size() + 1, 0);
	}

	Sleep(500);
	
	
}

void CommHandler::syncGame(User* user, Server* serv)//sync all the info from the server to the clients
{
	//Copy pasted code from start game
	cout << user->getname() << " synced game" << endl;
	string msg;
	Room* curr = getRoomIn(user->getRoom(), serv);
	Sleep(500);
	msg = "@210|" + curr->getCard().getype() + "|" + curr->getUsers()[curr->getTurn()]->getname() + "||";
	for (int i = 0; i < curr->getSize(); i++)//210
	{
		send(curr->getUsers()[i]->getsoc(), (msg).c_str(), msg.size() + 1, 0);
	}
	Sleep(500);
	
}

Card CommHandler::getRandomCard()
{
	vector<Card> cards;
	string red = "r", green = "g", blue = "b", yellow = "y", plus2 = "$", stop = "!", rotate = "<", plus = "+", changeC = "%", taki = "^", supertaki = "*";
	for (int j = 0; j < 2; j++)
	{
		for (int i = 1; i < 10; i++)
		{
			if (i == 2)
				continue;
			cards.push_back(Card(to_string(i) + red));
			cards.push_back(Card(to_string(i) + green));
			cards.push_back(Card(to_string(i) + blue));
			cards.push_back(Card(to_string(i) + yellow));
		}
	}
	for (int i = 0; i < 2; i++)
	{
		cards.push_back(Card(plus2 + red));
		cards.push_back(Card(plus2 + green));
		cards.push_back(Card(plus2 + blue));
		cards.push_back(Card(plus2 + yellow));

		cards.push_back(Card(stop + red));
		cards.push_back(Card(stop + green));
		cards.push_back(Card(stop + blue));
		cards.push_back(Card(stop + yellow));

		cards.push_back(Card(rotate + red));
		cards.push_back(Card(rotate + green));
		cards.push_back(Card(rotate + blue));
		cards.push_back(Card(rotate + yellow));

		cards.push_back(Card(plus + red));
		cards.push_back(Card(plus + green));
		cards.push_back(Card(plus + blue));
		cards.push_back(Card(plus + yellow));

		cards.push_back(Card(changeC + red));
		cards.push_back(Card(changeC + green));
		cards.push_back(Card(changeC + blue));
		cards.push_back(Card(changeC + yellow));

		cards.push_back(Card(taki + red));
		cards.push_back(Card(taki + green));
		cards.push_back(Card(taki + blue));
		cards.push_back(Card(taki + yellow));

		cards.push_back(Card(supertaki + red));
		cards.push_back(Card(supertaki + green));
		cards.push_back(Card(supertaki + blue));
		cards.push_back(Card(supertaki + yellow));
	}

	random_shuffle(cards.begin(), cards.end());
	int num = rand() % cards.size();
	return cards[num];
}

void CommHandler::drawCard(User* user, Server* serv)
{
	int drawCount = getRoomIn(user->getRoom(), serv)->drawCount;
	cout << "DrawCard: drawCount = " + std::to_string(drawCount);
	if (drawCount == 0)
	{
		Card temp = getRandomCard();
		user->pushCard(temp);
		send(user->getsoc(), ("@201|" + temp.getype() + "||").c_str(), temp.getype().length() + 1 + 7, 0);
		getRoomIn(user->getRoom(), serv)->advanceTurn(1);
		return;
	}
	for (int i = 0; i < drawCount; i++)
	{
		Card temp = getRandomCard();
		user->pushCard(temp);
		send(user->getsoc(), ("@201|" + temp.getype() + "||").c_str(), temp.getype().length() + 1 + 7, 0);
	}
	string msg = "@211|" + std::to_string(drawCount);

	getRoomIn(user->getRoom(), serv)->drawCount = 0;
	getRoomIn(user->getRoom(), serv)->advanceTurn(1);
	User* next = getRoomIn(user->getRoom(), serv)->getUsers()[getRoomIn(user->getRoom(), serv)->getTurn()];
	msg += next->getname();
	msg += "||";
	//Send 211
	for (int i = 0; i < getRoomIn(user->getRoom(), serv)->getSize(); i++)
	{
		send(getRoomIn(user->getRoom(), serv)->getUsers()[i]->getsoc(), msg.c_str(), msg.length() + 1, 0);
	}
}