#include "Room.h"

vector<string> getCardsList(string cards)
{
	cout << "In getCardsList, cards = " + cards << endl;
	int amount = (cards.length() / 3) + 1;
	vector<string> cardList(amount);
	for (int i = 0; i < amount; i++)
	{
		string currCard = cards.substr(0, 2);
		cardList[i] = currCard;
		if (cards.length() > 3)
			cards = cards.substr(3, cards.length() - 3);
	}
	return cardList;
}

void Room::attemptPlay(User user,string cards)
{
	int legal = 0;
	int currPlayerAttempt = 0;
	for (int i = 0; i < this->getUsers().size(); i++)//who tried
	{
		if (this->getUsers()[i]->getname() == user.getname())
		{
			currPlayerAttempt = i;
			break;
		}
	}

	vector<string> cardList = getCardsList(cards);
	unsigned int i = 0;
	if (currPlayerAttempt == currTurn) //its legal turn
	{
		std::cout << "Correct player's turn" << endl;
		for (i = 0; i < cardList.size(); i++)
		{

			legal = is_turn_legal(cardList[i]);
			
			if (this->taki == true)
			{
				if (legal == -1)
				{
					this->setCard(cardList[i]);
					this->getUsers()[currPlayerAttempt]->deleteCard(Card(cardList[i]));
					break;
				}
			}
			if (legal != -1)
			{
				std::cout << "card is legal: " << cardList[i] << endl;
				switch (legal)
				{
				case 0: //success
					this->setCard(cardList[i]);
					break;
				case -2://stop
					this->setCard(cardList[i]);
					this->advanceTurn(1);
					break;
				case -3://plus
					this->setCard(cardList[i]);
					break;
				case -4://taki
					this->setCard(cardList[i]);
					this->taki = true;
					break;
				case -5://rotate
					this->switchRotate();
					break;
				};
				this->getUsers()[currPlayerAttempt]->deleteCard(Card(cardList[i]));
				continue;
			}
			else
			{
				std::cout << "error: " << lastComment << "\n";
			}
		}
		this->taki = false;
	}

	

	if (i == cardList.size())
	{
		this->advanceTurn(1);
		string nextPlayerName = this->getUsers()[currTurn]->getname();
		string msg = "@200|" + nextPlayerName + "||";
		cout << "In attemptPlay: next player's name: " + nextPlayerName << endl;
		send(user.getsoc(), msg.c_str(), msg.length() + 1, 0);

		
		return;
	}
	else
		std::cout << this->getUsers()[currPlayerAttempt]->getname() << " failed to play" << endl;
}

int Room::getTurn()
{
	return this->currTurn;
}

void Room::advanceTurn(int num)
{
	if (this->rotate == false)
		this->currTurn = (this->currTurn-num)%this->users.size();
	else
		this->currTurn = (this->currTurn+num)%this->users.size();
}

bool Room::getRotate()
{
	return this->rotate;
}

void Room::switchRotate()
{
	if (this->rotate == false)
		this->rotate = true;
	else
		this->rotate = false;
}

Card Room::getCard()
{
	return this->_topCard;
}

void Room::setCard(Card card)
{
	this->_topCard = card;
}

bool Room::add_user(User* user)
{
	if (user->isLoggedIn() == false && this->isPremium == true)
		return false;
	if (this->users.size() < 4)
	{
		this->users.push_back(user);
		return true;
	}
	return false;
}

bool Room::GameState()
{
	return this->isInGame;
}

void Room::RunGame()
{
	this->isInGame = true;
}

int Room::getSize()
{
	return this->users.size();
}

string Room::getName()
{
	return this->roomname;
}

vector<User*> Room::getUsers()
{
	return this->users;
}

void Room::delete_user(User &user)
{
	unsigned int i;
	std::vector<User>::iterator newEnd;
	for (i = 0; i < this->users.size(); i++)
	{
		if (*(this->users[i]) == user)
		{
			break;
		}
	}
	this->users.erase(this->users.begin() + i);
}

void Room::add_user_lob(User* user)
{
	this->users.push_back(user);
	user->setRoom(this->getName());
}


/*return values:
0 - success
-1 - error and put the comment in 'lastComment'
-2 : stop
-3 : plus
-4 : taki
-5 : rotate
*/
int Room::is_turn_legal(Card _move)
{
	/*[0] - type | [1] - color*/
	string topCard = this->_topCard.getype();
	string moveCard = _move.getype();

	if (this->drawCount > 1)      //+2
	{
		if (moveCard[0] != '$')         //another +2?
		{
			this->lastComment = "need to draw 2 cards";
			return -1;
		}
		drawCount += 2;
		return 0;
	}

	if (moveCard[0] == '%') //CC
	{
		return 0;
	}

	if (moveCard[0] == '*')         //SUPAH TAKI
	{
		return -4;
	}

	if (moveCard[1] != topCard[1] && moveCard[0] != topCard[0])             //��� ���� ����
	{
		this->lastComment = "card can't be played";
		return -1;
	}

	switch (moveCard[0])
	{
		case '+':
			return -3;      //plus
			break;
		case '!':
			return -2;      //stop
			break;
		case '<':
			return -5;      //rotate
			break;
		case '$':
			this->drawCount += 2;
			return 0;       //+2
			break;
		case '^':
			return -4;      //taki
			break;
		default:       //any numeric card
			return 0;
			break;
	};
}

void Room::sendCards()
{
	string msg = "";
	for (int i = 0; i < this->getSize(); i++)
	{
		msg = "@201|";
		for (int j = 0; j < this->users[i]->getCards().size(); j++)
		{
			msg += this->users[i]->getCards()[j].getype()+",";
		}
		msg += "||";
		msg.erase(msg.find_last_of(","));
		send(this->users[i]->getsoc(), (msg).c_str(), msg.size() + 1, 0);
	}
}

User* Room::getAdmin()
{
	return this->users[0];
}

Room::Room(string name, bool premium)
{
	this->currTurn = 0;
	this->taki = false;
	this->_topCard = Card("");
	this->isInGame = false;
	this->users.reserve(4);
	this->roomname = name;
	this->isPremium = premium;
	this->drawCount = 0;
}

Room::~Room()
{
}


