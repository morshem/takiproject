#include "User.h"


User::User(std::string name, SOCKET soc)
{
	this->username = name;
	this->soc = soc;
	this->roomname = "";
	this->verified = false;
}

User::User()
{
}

std::string User::getname()
{
	return this->username;
}
SOCKET User::getsoc()
{
	return this->soc;
}
bool User::operator==(User otheru)
{
	if (this->soc == otheru.soc)
		return true;
	else
		return false;
}
void User::setRoom(string room)
{
	this->roomname = room;
}
string User::getRoom()
{
	return this->roomname;
}
void User::login(std::string name)
{
	this->verified = true;
	this->username = name;
	std::cout << name << " is logged in with " << this->getsoc() << " soc" <<std::endl;
}

void User::pushCard(Card card)
{
	this->cards.push_back(card);
}

void User::deleteCard(Card card)
{
	int index = 0;
	for (int i = 0; i < this->cards.size(); i++)
	{
		if (this->cards[i] == card)
		{
			index = i;
			break;
		}
	}
	this->cards.erase(this->cards.begin() + index);
}


bool User::isLoggedIn()
{
	return this->verified;
}


std::vector<Card> User::getCards()
{
	return this->cards;
}
User::~User()
{
}
