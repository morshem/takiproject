#define _WINSOCK_DEPRECATED_NO_WARNINGS
#pragma comment(lib,"ws2_32.lib")
#include <stdlib.h>
#include <mutex>
#include "Server.h"
#include <time.h>
#include "CommHandler.h"

using namespace std;

#define MESSAGE_SIZE 1024

std::mutex myLock;


int sql_callback(void* sqlAns, int argc, char** argv, char** azCol);

Server::Server(int port)
{
	srand(time(NULL));
	this->maxrooms = 20;
	sqlite3_open("Taki.db", &db);	//open database
	this->_port = port;
	
	if (WSAStartup(MAKEWORD(2, 0), &_info))
	{
		throw "WSAStartup failed";
	}

	_socket = socket(AF_INET, SOCK_STREAM, 0);
	if (_socket == INVALID_SOCKET)
	{
		WSACleanup();
		throw "socket failed";
	}

	_addi.sin_family = AF_INET;
	_addi.sin_addr.s_addr = INADDR_ANY;
	_addi.sin_port = htons(_port);

	if (::bind(_socket, (struct sockaddr *)&_addi, sizeof(struct sockaddr_in)) < 0)
	{
		closesocket(_socket);
		WSACleanup();
		throw "bind failed";
	}


	std::cout << "Bind succeeded" << std::endl;

}

void Server::sql_select(const char* sql)
{
	this->sqlAns = "";
	char* zErrMsg;
	int	rc = sqlite3_exec(this->db, sql, sql_callback, &(Server::sqlAns), &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
	}
}

int sql_callback(void* sqlAns, int argc, char** argv, char** azCol)
{
	int i;


	for (i = 0; i < argc; i++)
	{
		string* stringSqlAns = (string*)sqlAns;
		*stringSqlAns = *stringSqlAns + azCol[i] + " = " + argv[i] + "\n";
	}

	return 0;
}

Server::~Server()
{
	closesocket(_socket);
	sqlite3_close(db);
	WSACleanup();
}



bool Server::Listen()
{
	if (listen(_socket, SOMAXCONN) == SOCKET_ERROR)
	{
		std::cout << "Error in listening" <<WSAGetLastError()<< std::endl;
		return false;
	}

	std::cout << "listening to port " << _port << " ..." << std::endl;
	return true;
}



std::thread* Server::startAccept()
{
	int x = sizeof(struct sockaddr_in);
	sockaddr  myclient;
	SOCKET clientsocket = accept(_socket, (sockaddr *)&myclient, &x);
	User* newus = new User(std::to_string(clientsocket), clientsocket);
	newus->setRoom(this->rooms[0].getName());
	this->rooms[0].add_user(newus);
	CommHandler::handleMsg(newus, "@111| ||", this);
	std::thread* t = new std::thread(&Server::handleClient, this, newus);
	return t;
}

void Server::handleClient(User* curruser)
{
	struct sockaddr_in addr;
	int addr_size = sizeof(struct sockaddr_in);
	int res = getpeername(curruser->getsoc(), (struct sockaddr *)&addr, &addr_size);
	char rcmsg[1024] = "";
	std::cout << "Handle client: socket=" << curruser->getsoc() << ", ip=" << inet_ntoa(addr.sin_addr) << std::endl;
	myLock.lock();
	this->cleanEmptyRoom();
	myLock.unlock();
	while (1)
	{
		int received;
		if ((received = recv(curruser->getsoc(), rcmsg, 1024, 0)) <= 0)
		{
			this->getRoomIn(curruser->getRoom())->delete_user(*curruser);
			closesocket(curruser->getsoc());
			std::cout << "disconected: socket=" << curruser->getsoc() << ", ip=" << inet_ntoa(addr.sin_addr) << std::endl;
			return;
		}
		rcmsg[received] = 0;
		std::cout << rcmsg << endl;
		CommHandler::handleMsg(curruser, rcmsg, this);
		rcmsg[0] = '\n';
	}
}

void Server::cleanEmptyRoom()
{
	int size = this->rooms.size();
	for (int i = 1; i < size; i++)//i=1 because 0 is lobby and it is meant to be empty sometimes
	{
		if (this->rooms[i].getSize() == 0)
		{
			cout << this->rooms[i].getName() << " got delete" << endl;
			this->rooms.erase(this->rooms.begin() + i);
			size--;
			i--;
		}
	}
	return;
}
Room* Server::getRoomIn(string name)
{
	for (int i = 0; i < this->rooms.size(); i++)
	{
		if (this->rooms[i].getName() == name)
			return &(this->rooms[i]);
	}
	return nullptr;
}