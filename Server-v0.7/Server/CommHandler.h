#pragma once

#include "Server.h"
#include <algorithm>

//Is in charge of all the sending and recieving of data from clients
class CommHandler//short for communist handler
{
private:
	static Room* getRoomIn(string name, Server* serv);
	static void sendMsg(Msg msgSent, Server* serv);
	static void sendRoomList(User* user, Server* serv);
	static bool addToRoom(User* user, string name, Server* serv);
	static void attemptLogin(User* user, string name_pass, Server* serv);
	static void attemptRegister(User* user, string name_pass, Server* serv);
	static void createNewRoom(User* user, string name, Server* serv);
	static void sendUserList(User user, Server* serv);
	static void startGame(User* user,Server* serv);
	static void syncGame(User* user, Server* serv);
	static Card getRandomCard();
	static void drawCard(User* user, Server* serv);


public:
	static void handleMsg(User* user, string msg, Server* serv);
	CommHandler();
	~CommHandler();
};

