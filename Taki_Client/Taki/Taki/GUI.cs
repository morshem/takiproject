﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Windows;
using System.Threading;
using Microsoft.VisualBasic;

namespace Taki
{
    public partial class GUI : Form 
    {
        private List<Card> cardQueue;
        private Player mainPlayer;
        private System.Windows.Forms.Label[,] otherPlayerLabels;
        Thread flashT;

        public Messanger mainHandler;

        public GUI()
        {
            otherPlayerLabels = new System.Windows.Forms.Label[3,2];

            InitializeComponent();

            flashT = new Thread(() => this.flash());
            mainPlayer = new Player("");
            cardQueue = new List<Card>();


            //Get IP Address from user and start the messanger!
            Console.WriteLine("Enter the IP address of the server:");

            string serverIP = "127.0.0.1";//Console.ReadLine();
            System.Net.IPAddress lel;
            while(!IPAddress.TryParse(serverIP, out lel))
            {
                Console.WriteLine("YOU DONE FUCKED UP NOW, NIGGA!\nTry again...");
                serverIP = Console.ReadLine();
            }
            mainHandler = new Messanger(serverIP, 3772, this.mainPlayer, this);

        }

        #region Event Handlers

        //initializes the Listener thread
        private void beginListening_Click(object sender, EventArgs e)
        {
            Thread listenThread = new Thread(() => Listener(mainHandler));
            listenThread.Start();

            toLobbyScreen();

            beginListening.Enabled = false;
            beginListening.Visible = false;
        }

        //Sends the server a chat massage and resets writeChat
        private void writeChat_KeyPress(object sender, KeyPressEventArgs e)
        {
           if(e.KeyChar == (char)Keys.Return)
           {
               Protocol_Client.CH_SEND(writeChat.Text);
               console.AppendText("Sending a chat massage " + writeChat.Text.Length + " characters long" + Environment.NewLine);
               chat.AppendText("Me: " + writeChat.Text);
               writeChat.Text = "";

           }

        }

        private void getRoomList_Click(object sender, EventArgs e)
        {
            Protocol_Client.RM_ROOM_LIST();
            writeToConsole("Requesting room list");
        }

        //Sends login request, updates the console and resets the username and password field
        private void login_Click(object sender, EventArgs e)
        {
            Protocol_Client.EN_LOGIN(userName.Text, password.Text);
            console.AppendText("Sending login request for:" + Environment.NewLine);
            console.AppendText("Username- " + userName.Text + Environment.NewLine);
            console.AppendText("Password- " + password.Text + Environment.NewLine);

            mainPlayer = new Player(userName.Text);

            userName.Text = "";
            password.Text = "";
        }

        //Registers, updates the console and resets the username and password fields
        private void register_Click(object sender, EventArgs e)
        {
            Protocol_Client.EN_REGISTER(userName.Text, password.Text);
            console.AppendText("Sending registeration request for:" + Environment.NewLine);
            console.AppendText("Username- " + userName.Text + Environment.NewLine);
            console.AppendText("Password- " + password.Text + Environment.NewLine);

            mainPlayer = new Player(userName.Text);

            userName.Text = "";
            password.Text = "";
        }

        //Asks for room name
        private void createRoomButton_Click(object sender, EventArgs e)
        {
            string roomName = Interaction.InputBox("Room:", "Enter room name:", "idk LOL", -1, -1);
            Protocol_Client.RM_CREATE_GAME(roomName);
            console.AppendText("Requested a room creation with name: " + roomName + "\n");
        }

        //Sends a logout request and updates the console
        private void logout_Click(object sender, EventArgs e)
        {
            Protocol_Client.EN_LOGOUT();
            console.AppendText("Logging out" + Environment.NewLine);
        }

        //If in-game (indicated by locked buttons) exits the game and requests the room list
        private void toLobby_Click(object sender, EventArgs e)
        {
            if (!login.Enabled)//If you are inside a game/room
            {
                Protocol_Client.RM_LEAVE_GAME();
                console.AppendText("Exiting game" + Environment.NewLine);
                login.Enabled = true;
                register.Enabled = true;
                toLobby.Enabled = false;
            }

            toLobbyScreen();
            roomListBox.Items.Clear();
            Protocol_Client.RM_ROOM_LIST();
            console.AppendText("Requesting room list" + Environment.NewLine);
        }

        //Sends GM_DRAW, but cheks if you played any cards
        private void drawButton_Click(object sender, EventArgs e)
        {
            if(cardQueue.Count != 0)
            {
                MessageBox.Show("You tried to play some cards, cheecky cunt.");
                return;
            }
            

            Protocol_Client.GM_DRAW();
        }

        //If cardsQueue is not empty, send the cards to the server
        private void endTurnButton_Click(object sender, EventArgs e)
        {
            if (cardQueue.Count == 0)
            {
                MessageBox.Show("You didn't try to play any cards, you cheecky cunt. ʘ‿_0");
                return;
            }    

            string cardsString = "";
            for(int i=0; i < cardQueue.Count; i++)
            {
                cardsString += cardQueue[i].GetValue().ToString() + cardQueue[i].GetColor().ToString() + ",";
            }
            cardsString = cardsString.Substring(0, cardsString.LastIndexOf(","));

            Protocol_Client.GM_PLAY(cardsString);

            chooseColorList.Visible = false;
            chooseColorList.Enabled = false;

            console.AppendText("Sending a played hand:" + Environment.NewLine);
            console.AppendText(cardsString + Environment.NewLine);
            
        }

        //resets the cardsQueueBox and cardQueue list
        private void resetHandButton_Click(object sender, EventArgs e)
        {
            cardsQueueBox.Items.Clear();
            cardQueue.Clear();
        }

        private void hand_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (hand.DroppedDown && hand.SelectedItem != null)
            {
                cardsQueueBox.Items.Add(hand.SelectedItem);
                string choice = hand.SelectedItem.ToString();
                cardQueue.Add(mainPlayer.findByDisplay(hand.SelectedItem.ToString()));
                hand.Items.Remove(hand.SelectedItem);

                if(choice[1] == 'X')
                {
                    if(chooseColorList.Items.Count != 4)
                    {
                        chooseColorList.Items.Add("Blue");
                        chooseColorList.Items.Add("Green");
                        chooseColorList.Items.Add("Yellow");
                        chooseColorList.Items.Add("Red");
                    }

                    endTurnButton.Enabled = false;
                    drawButton.Enabled = false;
                    hand.Enabled = false;
                    chooseColorList.Visible = true;
                    chooseColorList.Enabled = true;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            toGameScreen();
        }

        private void chooseColorList_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (chooseColorList.DroppedDown && chooseColorList.SelectedItem != null)
            {
                string choice = chooseColorList.SelectedItem.ToString();
                console.AppendText(choice);
                char color = 'X';
                switch (choice)
                {
                    case "Blue":
                        color = 'b';
                        break;
                    case "Green":
                        color = 'g';
                        break;
                    case "Yellow":
                        color = 'y';
                        break;
                    case "Red":
                        color = 'r';
                        break;
                }
                cardQueue.RemoveAt(cardQueue.Count - 1);
                cardQueue.Insert(cardQueue.Count, new Card('%', color));

                chooseColorList.Enabled = false;
                chooseColorList.Visible = false;

                hand.Enabled = true;
                drawButton.Enabled = true;
                endTurnButton.Enabled = true;
            }
        }

        private void roomListBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Protocol_Client.RM_JOIN_GAME(roomListBox.SelectedItem.ToString());
        }

        private void startGameButton_Click(object sender, EventArgs e)
        {
            Protocol_Client.RM_START_GAME();
            startGameButton.Visible = false;
            startGameButton.Enabled = false;
        }

        #endregion

        #region Utility 
        //Updating/linking the GUI to the logistics

        public void writeToChat(string toWrite)
        {
            chat.Invoke((MethodInvoker)delegate { chat.AppendText(toWrite + Environment.NewLine); });
        }
        public void writeToConsole(string toWrite)
        {
            console.Invoke((MethodInvoker)delegate { console.AppendText(toWrite + Environment.NewLine); }); 
        }
        private int findNextPlayerIndex(System.Windows.Forms.Label[,] otherPlayerLabels)
        {
            for (int i = 0; i < otherPlayerLabels.Length; i++)
            {
                if (otherPlayerLabels[i, 0] == null)
                {
                    return i;
                }
            }
            return -1;

        }
        private void updateTopCard(string display)
        {
            char value = display[0];
            char color = display[1];

            switch (value)
            {
                case Card.ONE:
                case Card.THREE:
                case Card.FOUR:
                case Card.FIVE:
                case Card.SIX:
                case Card.SEVEN:
                case Card.EIGHT:
                case Card.NINE:
                case Card.PLUS:
                case Card.STOP:
                    topCard.Text = value.ToString();
                    topCard.Font = new Font("Lucida Sans", 56);
                    break;
                case Card.PLUS_TWO:
                    topCard.Font = new Font("Lucida Sans", 28);
                    topCard.Text = " +\n 2";
                    break;
                case Card.CHANGE_COLOR:
                    topCard.Font = new Font("Lucida Sans", 14);
                    topCard.Text = "CHANGE\nCOLOR";
                    break;
                case Card.CHANGE_DIRECTION:
                    topCard.Font = new Font("Lucida Sans", 28);
                    topCard.Text = " <\n >";
                    break;
                case Card.TAKI:
                    topCard.Font = new Font("Lucida Sans", 28);
                    topCard.Text = "TA\nK I";
                    break;
                case Card.SUPER_TAKI:
                    topCard.Font = new Font("Lucida Sans", 20);
                    topCard.Text = "SUPER\n TAKI";
                    break;
            }

            switch (color)
            {
                case 'g':
                    topCard.ForeColor = System.Drawing.Color.Green;
                    break;
                case 'b':
                    topCard.ForeColor = System.Drawing.Color.Blue;
                    break;
                case 'y':
                    topCard.ForeColor = System.Drawing.Color.Yellow;
                    break;
                case 'r':
                    topCard.ForeColor = System.Drawing.Color.Red;
                    break;
                case 'X':
                    topCard.ForeColor = System.Drawing.Color.Magenta;
                    break;
            }
        }
        private void updateHandList(List<Card> playerHand)
        {
            Console.WriteLine("HELLO: In updateHandList");
            hand.Items.Clear();
            for(int i = 0; i < playerHand.Count; i++)
            {
                Console.WriteLine(playerHand[i].getDisplay());//debug
                hand.Items.Add(playerHand[i].getDisplay());
            }
        }
        private void updateRoomList(string[] roomList)
        {
            roomListBox.Items.Clear();
            for(int i = 0; i < roomList.Length; i++)
            {
                roomListBox.Items.Add(roomList[i]);
            }
        }
        public void toLobbyScreen()
        {
            //Clear all internal saved values
            hand.Items.Clear();
            cardQueue.Clear();
            cardsQueueBox.Items.Clear();
            mainPlayer.clearHand();
            topCard.Text = "X";

            //hand list
            hand.Visible = false;
            hand.Enabled = false;

            //player labels
            player1.Visible = false;
            player1.Enabled = false;
            cardCount1.Visible = false;
            cardCount1.Enabled = false;
            
            
            //Top card & Color choosing list
            topCard.Visible = false;
            topCard.Enabled = false;
            chooseColorList.Visible = false;
            chooseColorList.Enabled = false;

            //Card Queue box & reset
            cardsQueueBox.Visible = false;
            cardsQueueBox.Enabled = false;
            resetHandQueue.Visible = false;
            resetHandQueue.Enabled = false;

            //Playing buttons
            endTurnButton.Visible = false;
            endTurnButton.Enabled = false;
            drawButton.Visible = false;
            drawButton.Enabled = false;

            //Create room
            createRoomButton.Enabled = true;
            createRoomButton.Visible = true;
            getRoomList.Visible = true;
            getRoomList.Enabled = true;
            roomListBox.Visible = true;
            roomListBox.Enabled = true;
            toLobby.Enabled = false;
            button1.Enabled = true;

            //420 BlazeIt
            lobbyGif.Visible = true;
            lobbyGif.Enabled = true;
            if (!flashT.IsAlive)
                flashT.Start();
            else
                flashT.Resume();

            //RoomList/StartGame Button
            getRoomList.Visible = true;
            getRoomList.Enabled = true;
            startGameButton.Visible = false;
            startGameButton.Enabled = false;

        }
        private void toGameScreen()
        {
            //hand list
            hand.Visible = true;
            hand.Enabled = true;

            //player labels
            player1.Visible = true;
            player1.Enabled = true;
            cardCount1.Visible = true;
            cardCount1.Enabled = true;
           

            //Top card & Color choosing list
            topCard.Visible = true;
            topCard.Enabled = true;
            chooseColorList.Visible = false;
            chooseColorList.Enabled = false;

            //Card Queue box & reset
            cardsQueueBox.Visible = true;
            cardsQueueBox.Enabled = true;
            resetHandQueue.Visible = true;
            resetHandQueue.Enabled = true;

            //Create room
            createRoomButton.Enabled = false;
            createRoomButton.Visible = false;
            getRoomList.Visible = false;
            getRoomList.Enabled = false;
            roomListBox.Visible = false;
            roomListBox.Enabled = false;
            toLobby.Enabled = true;
            button1.Enabled = false;

            //Playing buttons
            endTurnButton.Visible = true;
            endTurnButton.Enabled = true;
            drawButton.Visible = true;
            drawButton.Enabled = true;

            //Gif, duh
            lobbyGif.Visible = false;
            lobbyGif.Enabled = false;

            flashT.Suspend();
            smokeWeed.Visible = false;
            smokeWeed.Enabled = false;

            //RoomList/StartGame Button
            getRoomList.Visible = false;
            getRoomList.Enabled = false;
            startGameButton.Visible = true;
            startGameButton.Enabled = true;
        }
        private void flash()
        {
            while(true)
            {
                smokeWeed.Invoke((MethodInvoker)delegate
                {
                    smokeWeed.Visible = smokeWeed.Visible ^ true;
                    smokeWeed.Enabled = true;
                    if (smokeWeed.ForeColor != System.Drawing.Color.Fuchsia)
                        smokeWeed.ForeColor = System.Drawing.Color.Fuchsia;
                    else
                        smokeWeed.ForeColor = System.Drawing.Color.Gold;
                });
                Thread.Sleep(500);
            }
        }
        static private string[] getArguments(string response)//da real mvp
        {
            string[] arguments = new string[0];
            response = response.Substring(5);//clean the "@xxx|" from the string\
            if (response.Length != 0)
            {
                for (int i = 0; response.IndexOf("||") > 0; i++)
                {
                    Array.Resize<string>(ref arguments, arguments.Length + 1);
                    arguments[i] = response.Substring(0, response.IndexOf("|"));
                    response = response.Substring(response.IndexOf("|") + 1);
                }
                Console.WriteLine("Arguments retrieved:");
                for (int i = 0; i < arguments.Length; i++ )
                    Console.WriteLine(arguments[i]);
                return arguments;
            }
            return null;
        }

        #endregion

        #region Server response messages handlers

        //This is the thread that will run in the background and handle all server responses and announcments
        public void Listener(Taki.Messanger msn)
        {
            int code;
            string response;

            while (true)
            {
                try
                {  
                     response = Messanger.getMessageFromServer();
                     code = Messanger.getCode(response);

                    writeToChat(response + "\n");
                    Console.WriteLine(response);

                    switch (code)
                    {
                        //Program Messages:
                        //Success-
                        case 100:
                            PGM_SCC_LOGIN(response);
                            break;
                        case 101:
                            PGM_SCC_REGISTER(response);
                            break;
                        case 102:
                            PGM_SCC_GAME_CREATED(response);
                            break;
                        case 103:
                            PGM_SCC_GAME_JOIN(response);
                            break;
                        case 104:
                            PGM_SCC_GAME_CLOSED(response);
                            break;
                        case 105:
                            PGM_SCC_LOGOUT(response);
                            break;
                        case 106:
                            PGM_SCC_GAME_LEFT(response);
                            break;
                        //Control-
                        case 110:
                            PGM_CTR_NEW_USER(response);
                            break;
                        case 111:
                            PGM_CTR_REMOVE_USER(response);
                            break;
                        case 112:
                            PGM_CTR_GAME_STARTED(response);
                            break;
                        case 113:
                            PGM_CTR_ROOM_CLOSED(response);
                            break;
                        case 114:
                            PGM_CTR_ROOM_LIST(response);
                            break;

                        //Game messages:
                        //Success-
                        case 200:
                            GAM_SCC_TURN(response);
                            break;
                        case 201:
                            GAM_SCC_DRAW(response);
                            break;
                        //Control-
                        case 210:
                            GAM_CTR_TURN_COMPLETE(response);
                            break;
                        case 211:
                            GAM_CTR_DRAW_CARDS(response);
                            break;
                        case 212:
                            GAM_CTR_GAME_ENDED(response);
                            break;
                        case 213:
                            string card = getArguments(response)[0];
                            updateTopCard(card);
                            break;

                        //Chat messages:
                        //Success-
                        case 300:
                            CHA_SCC(response);
                            break;
                        case 310:
                            CHA_MSG(response);
                            break;

                        case 999:
                            GAM_CTR_GAME_ENDED("");
                            break;

                        default:
                            writeToConsole(GUI.ProtocolErrorReader(response, code));
                            break;
                    }
                }
                catch (Exception s)
                {
                    Console.WriteLine(s.Message);
                }
            }
        }

        //1XX messages:

        //10X Success-
        public void PGM_SCC_LOGIN(string roomList)//roomList is the untouched string as received from the server after a succesfull login
        {
            
            string[] rooms = getArguments(roomList);
            roomListBox.Invoke((MethodInvoker)delegate { updateRoomList(rooms); });
            writeToChat("Logged in successfully");
        }
        public void PGM_SCC_REGISTER(string roomList)//same is PGM_SCC_LOGIN
        {
            writeToConsole("Registration complete!\nHere are the rooms available:" + getArguments(roomList));
            string[] rooms = getArguments(roomList);
            roomListBox.Invoke((MethodInvoker)delegate { updateRoomList(rooms); });
        }
        public void PGM_SCC_GAME_CREATED(string response)
        {
            writeToConsole("Room created succesfully!");
            updateRoomList(getArguments(response));
        }
        public void PGM_SCC_GAME_JOIN(string playerList)
        {
            Invoke((MethodInvoker) delegate{
            string[] otherPlayers = getArguments(playerList);
            for(int i = 0; i < otherPlayers.Length; i++)
            {
                //Name label
                otherPlayerLabels[i, 0] = new System.Windows.Forms.Label();

                otherPlayerLabels[i, 0].AutoSize = true;
                otherPlayerLabels[i, 0].Location = new System.Drawing.Point(289, 245 + (i+1)*22);//offset the label
                otherPlayerLabels[i, 0].Name = "player " + i.ToString() + 1.ToString();
                otherPlayerLabels[i, 0].Size = new System.Drawing.Size(27, 13);
                otherPlayerLabels[i, 0].TabIndex = 9;
                otherPlayerLabels[i, 0].Text = otherPlayers[i];

                otherPlayerLabels[i, 0].Visible = true;

                //Card count label
                otherPlayerLabels[i, 1] = new System.Windows.Forms.Label();
                                     
                otherPlayerLabels[i, 1].AutoSize = true;
                otherPlayerLabels[i, 1].Location = new System.Drawing.Point(330, 245 + (i + 1) * 22);//offset the label
                otherPlayerLabels[i, 1].Name = "player " + i.ToString() + 1.ToString() + "card count";
                otherPlayerLabels[i, 1].Size = new System.Drawing.Size(27, 13);
                otherPlayerLabels[i, 1].TabIndex = 9;
                otherPlayerLabels[i, 1].Text = "8";

                otherPlayerLabels[i, 1].Visible = true;
            }

            player1.Text = mainPlayer.GetName();
            player1.Visible = true;
            cardCount1.Text = "8";


            toGameScreen();
            });
            
        }
        public void PGM_SCC_GAME_CLOSED(string response)
        {
            writeToConsole("Room closed successfully");
            toLobbyScreen();
        }
        public void PGM_SCC_LOGOUT(string response) //code 105
        {
            writeToConsole("Logged out successfully!");
        }
        public void PGM_SCC_GAME_LEFT(string response) //code 106
        {
            writeToConsole("Left the game - Clearing components");
            hand.Invoke((MethodInvoker)delegate { hand.Items.Clear(); });
        }

        //11X Control-
        public void PGM_CTR_NEW_USER(string response)
        {
            Invoke((MethodInvoker)delegate
            {
                string newPlayer = getArguments(response)[0];
                int newIndex = findNextPlayerIndex(otherPlayerLabels);

                //Name label
                otherPlayerLabels[newIndex, 0] = new System.Windows.Forms.Label();

                otherPlayerLabels[newIndex, 0].AutoSize = true;
                otherPlayerLabels[newIndex, 0].Location = new System.Drawing.Point(289, 245 + (newIndex + 1) * 22);//offset the label
                otherPlayerLabels[newIndex, 0].Name = "player " + newIndex.ToString() + 1.ToString();
                otherPlayerLabels[newIndex, 0].Size = new System.Drawing.Size(27, 13);
                otherPlayerLabels[newIndex, 0].TabIndex = 9;
                otherPlayerLabels[newIndex, 0].Text = newPlayer;

                //Card count label
                otherPlayerLabels[newIndex, 1] = new System.Windows.Forms.Label();

                otherPlayerLabels[newIndex, 1].AutoSize = true;
                otherPlayerLabels[newIndex, 1].Location = new System.Drawing.Point(330, 245 + (newIndex + 1) * 22);//offset the label
                otherPlayerLabels[newIndex, 1].Name = "player " + newIndex.ToString() + 1.ToString() + "card count";
                otherPlayerLabels[newIndex, 1].Size = new System.Drawing.Size(27, 13);
                otherPlayerLabels[newIndex, 1].TabIndex = 9;
                otherPlayerLabels[newIndex, 1].Text = "8";
            });

        }
        public void PGM_CTR_REMOVE_USER(string response)
        {
            string nameRemoved = getArguments(response)[0];
            for(int i = 0; i < otherPlayerLabels.GetLength(0); i++)
            {
                if(otherPlayerLabels[i,0].Text == nameRemoved)
                {
                    Invoke((MethodInvoker)delegate
                    {
                        otherPlayerLabels[1, 0].Dispose();
                        otherPlayerLabels[1, 1].Dispose();
                    });
                }
            }
        }
        public void PGM_CTR_GAME_STARTED(string response)
        {
            string[] start = getArguments(response);

            //My cards:
            string cardsDrawnString = start[0];
            List<Card> drawn = Card.getListFromString(cardsDrawnString);
            mainPlayer.addNewCards(drawn);

            hand.Invoke((MethodInvoker)delegate { updateHandList(mainPlayer.GetHand()); }); 

            //The top starting card
            string topCard = start[1];//already in the display format
            Invoke((MethodInvoker)delegate { updateTopCard(topCard); });
            Console.WriteLine("List: " + cardsDrawnString + "Top card: " + topCard);
        }
        public void PGM_CTR_ROOM_LIST(string response)
        {
            string[] rooms = getArguments(response);
            Invoke((MethodInvoker)delegate { updateRoomList(rooms); });
        }
        public void PGM_CTR_ROOM_CLOSED(string response)
        {
            writeToConsole("Room closed... punk ass bitch");
        }


        //2XX Game:

        //20X Success-
        public void GAM_SCC_TURN(string response)
        {
            Invoke((MethodInvoker)delegate
            {
                string nextPlayerToPlay = getArguments(response)[0];

                cardQueue.Clear();
                cardsQueueBox.Items.Clear();

                if (nextPlayerToPlay == this.mainPlayer.GetName())
                {
                    endTurnButton.Enabled = true;
                    resetHandQueue.Enabled = true;
                    drawButton.Enabled = true;
                    cardsQueueBox.Enabled = true;
                }
                else
                {
                    endTurnButton.Enabled = false;
                    resetHandQueue.Enabled = false;
                    drawButton.Enabled = false;
                    cardsQueueBox.Enabled = false;
                }

                if(mainPlayer.GetHand().Count == 0)
                {
                    Protocol_Client.sendToServer("@999||", "Error");
                }
            });
        }
        public void GAM_SCC_DRAW(string msg) //after draw the turn should end and i don't wanna touch it because I suck at threads fuck me
        {
            Invoke((MethodInvoker)delegate
            {
                string cardsDrawn = getArguments(msg)[0];
                mainPlayer.addNewCards(Card.getListFromString(cardsDrawn));
                updateHandList(mainPlayer.GetHand());
                writeToConsole(cardsDrawn);
                return;
            });
        }

        //21X Control-
        public void GAM_CTR_TURN_COMPLETE(string turnPlayed)
        {
            Invoke((MethodInvoker)delegate
            {
                List<Card> cardsPlayed = Card.getListFromString(getArguments(turnPlayed)[0]);
                updateTopCard(cardsPlayed[cardsPlayed.Count - 1].getDisplay());

                string nextPlayerToPlay = getArguments(turnPlayed)[1];

                cardQueue.Clear();
                cardsQueueBox.Items.Clear();
                Console.WriteLine("Turn complete. nextPlayerToPlay = " + nextPlayerToPlay + Environment.NewLine + "My name is: " + this.mainPlayer.GetName());
                if (nextPlayerToPlay == this.mainPlayer.GetName())
                {
                    endTurnButton.Enabled = true;
                    resetHandQueue.Enabled = true;
                    drawButton.Enabled = true;
                    cardsQueueBox.Enabled = true;
                }
                else
                {
                    endTurnButton.Enabled = false;
                    resetHandQueue.Enabled = false;
                    drawButton.Enabled = false;
                    cardsQueueBox.Enabled = false;
                }
            });
        }
        public void GAM_CTR_DRAW_CARDS(string amountCardsDrawn)
        {
            string[] response = getArguments(amountCardsDrawn);
            string playerName = response[0];
            string cardsDrawnAmount = response[1];

            for(int i = 0; i < otherPlayerLabels.Length; i++)
            {
                Invoke((MethodInvoker)delegate
                {
                    if (otherPlayerLabels[i, 0].Text == playerName)
                        otherPlayerLabels[i, 1].Text = (int.Parse(otherPlayerLabels[i, 1].Text) + int.Parse(cardsDrawnAmount)).ToString();
                });
            }
        }
        public void GAM_CTR_GAME_ENDED(string whoWonWhosNextYOUDECIDE)
        {
            string winner = getArguments(whoWonWhosNextYOUDECIDE)[0];
            writeToConsole("Game ended!\nRIP all the rest...");
            toLobbyScreen();
        }

        //3XX Chat:

        //30X Success-
        public void CHA_SCC(string response)
        {
            writeToConsole("Message sent successfully\n");
        }

        //31X Control
        public void CHA_MSG(string message)//replaces CHA_ERR - now contains the message itself
        {
            string msg = getArguments(message)[0];
            writeToChat(msg);
        }

        //Error parser
        static public string ProtocolErrorReader(string message, int code)
        {
            string errorType = message.Substring(message.IndexOf('|') + 1);
            string errorExplain = errorType.Substring(errorType.IndexOf('|') + 1);
            errorExplain = errorExplain.Substring(0, errorExplain.IndexOf('|'));
            errorType = errorType.Substring(0, errorType.IndexOf('|'));
            switch (code)
            {
                case 120: //login error
                    return "Error 120 - Can't login";
                case 121: //invalid parameters
                    return "Error 121 - Invalid info - " + errorType + " - " + errorExplain;
                case 122: //taken username
                    return "Error 122 - Invalid username - " + errorType + " is taken";
                case 123: //room is full
                    return "Error 123 - Invalid room - " + errorType + " is full";
                case 124: //room doesn't exist
                    return "Error 124 - " + errorType + " was not found";
                case 125: //cant start game with 1 player lol get some friends yuo scrub
                    return "Error 125 - Not enough players in the room";
                case 126: //sent info longer than max info length
                    return "Error 126 - Sent info is longer than the maximum length";
                case 130: //message doesn't fit the protocol
                    return "Error 130 - Message incompatible with the protocol";
                case 131: //the message is invlaid or you don't have access
                    return "Error 131 - Invalid message or insufficient access";
                case 220: //invalid card
                    return "Error 220 - Invalid card placement - " + errorType;
                case 221: //invalid card order (example: 6 then 9)
                    return "Error 221 - Invalid card placement order" + errorType;
                case 222: //turn ended with invalid card (example: + card)
                    return "Error 222 - Invalid last card placement";
                case 223: //wrong number of cards to draw were sent
                    return "Error 223 - Invalid card draw number was sent";
            }
            return "pfft";
        }

        #endregion             
        
    }

    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                GUI myGUI = new GUI();
                Application.Run(myGUI);
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Data);
                Console.WriteLine(e.Message);
            }
            


            Console.WriteLine("Hello@!");

            Console.ReadKey();
        }
    }

    public class Card
    {
        //Card values
        public const char ONE = '1', THREE = '3', FOUR = '4', FIVE = '5', SIX = '6', SEVEN = '7', EIGHT = '8', NINE = '9';
        public const char PLUS = '+';
        public const char STOP = '!';
        public const char PLUS_TWO = '$';
        public const char CHANGE_DIRECTION = '<';
        public const char CHANGE_COLOR = '%';
        public const char TAKI = '^';
        public const char SUPER_TAKI = '*';

        private char _type;
        private char _color;// If it is a colorless card will be X

        //Gets a list of actual "Card" class members from the string retrieved by the server
        public static List<Card> getListFromString(string cardString)//cards devided by ","
        {
            List<Card> cardsDrawn = new List<Card>(128);
            
            int i;
            Console.WriteLine(cardString.Length.ToString() + " Is the length of the cardString");
            for (i = 0; i < cardString.Length ;  i += 3)
            {
                cardsDrawn.Add( new Card(cardString[i], cardString[i + 1]));
            }
            Console.WriteLine("The string is: " + cardString + "The number of cards was: " + cardsDrawn.Count.ToString());
            return cardsDrawn;
        }

        //Getters
        public char GetValue() { return this._type; }
        public char GetColor() { return this._color; }
        public string getDisplay() { return this.GetValue().ToString() + this.GetColor().ToString(); }
        

        //Constructor
        public Card(char type, char color)
        {
            this._type = type;
            this._color = color;
        }
    }

    public class Player
    {
        private string name;
        private List<Card> hand;
        private int cardCount;//Will be .Count of hand

        public Player(string name)
        {
            hand = new List<Card>();
            this.name = name;
        }

        public void addNewCards(List<Card> drawn)
        {
            Console.WriteLine("HELLO: In addNewCards");
            for (int i = 0; i < drawn.Count; i++)
            {
                Console.WriteLine(drawn[i].getDisplay());
                hand.Add(drawn[i]);
            }
        }
        public void addNewCards(string cards)
        {
            List<Card> cardsL = Card.getListFromString(cards);
            addNewCards(cardsL);
        }

        public void clearHand()
        {
            this.hand.Clear();
        }

        public void useCards(List<Card> cardsUsed)
        {

        }

        //Getters
        public int GetCardCount() { return this.cardCount; }
        public string GetName() { return this.name; }
        public List<Card> GetHand() { return this.hand; }
        public Card findByDisplay(string display)//If not found returns null
        {
            for(int i = 0;  i < this.hand.Count; i++)
            {
                if(this.hand[i].getDisplay() == display)
                {
                    return this.hand[i];
                }
            }
            return null;
        }

    }

    /*
    This class has two main tasks - Textual chat room functionality
    and sending/receiving protocol messages from the server about:
    Logging in/out, signing up, game sequence etc...
    */
    public class Messanger
    {
        static private Socket server;
        private Player self;
        private bool canWaitForMessages;

        //Constructor, initializes the connection with the server
        public Messanger(string takiServerIP, int takiServerPort, Player ourPlayer, GUI gui)
        {
            try
            {
                TcpClient tempClient = new TcpClient(takiServerIP, takiServerPort);
                server = tempClient.Client;
                Protocol_Client.InitializeProtocol(server);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }

        
        public void disconnect()
        {
            server.Disconnect(false);
        }

        static public string getMessageFromServer()
        {
            byte[] reply = new byte[1024];
           
            server.Receive(reply);
            Console.WriteLine(toString(reply));
            string returned = toString(reply);
            returned = returned.Substring(0, returned.IndexOf("\0") + 1);
            return returned;
        }
        static public int getCode(string packet)
        {
            string rawCode = packet.Substring(1, packet.IndexOf("|") - 1);
            return Int32.Parse(rawCode);
        }
        static public byte[] toBytes(string str)
        {
            return Encoding.ASCII.GetBytes(str);
        }
        static public string toString(byte[] byt)
        {
            return System.Text.Encoding.UTF8.GetString(byt);
        }

    }

    /*
    Knows all protocol commands and what parameters to take
    */
    public class Protocol_Client
    {
        static public Socket ServerSocket;
        static public Object SocketLock;// Used for critical parts involving the socket with the server

        static public void InitializeProtocol(Socket server)
        {
            SocketLock = new Object();
            ServerSocket = server;
        }

        //X messages
        static public string EN_REGISTER(string userName, string password)
        {
            string message = "@1|" + userName + "|" + password + "||";

            return sendToServer(message, "EN_REGISTER");
        }
        static public string EN_LOGIN(string userName, string password)
        {
            string message = "@2|" + userName + "|" + password + "||";
            return sendToServer(message, "EN_LOGIN");
        }
        static public string EN_LOGOUT()
        {
            string message = "@3||";
            return sendToServer(message, "EN_LOGOUT");
        }

        //1X messages
        static public string RM_ROOM_LIST()
        {
            string message = "@10||";
            return sendToServer(message, "RM_ROOM_LIST");
        }
        static public string RM_CREATE_GAME(string name)
        {
            string message = "@11|" + name + "||";
            return sendToServer(message, "RM_CREATE_GAME");
        }
        static public string RM_JOIN_GAME(string adminName)
        {
            string message = "@12|" + adminName + "||";
            return sendToServer(message, "RM_JOIN_GAME");
        }
        static public string RM_START_GAME()
        {
            string message = "@13||";
            return sendToServer(message, "RM_START_GAME");
        }
        static public string RM_LEAVE_GAME()
        {
            string message = "@14||";
            return sendToServer(message, "RM_LEAVE_ROOM");
        }
        static public string RM_CLOSE_GAME()
        {
            string message = "@15||";
            return sendToServer(message, "RM_CLOSE_GAME");
        }

        //2X messages
        static public string GM_PLAY(string cards)//the cards are devided by "," except for the last one
        {
            string message = "@20|" + cards + "||";
            return sendToServer(message, "GM_PLAY");
        }
        static public string GM_DRAW()//Server sends card by the amount saved in "cardcount" that he calculated
        {
            string message = "@21||";
            return sendToServer(message, "GM_DRAW");
        }

        //3X messages
        static public string CH_SEND(string text)
        {
            string message = "@30|" + text + "||";
            return sendToServer(message, "CH_SEND");
        }

        //Repeating functionalities
        static public string sendToServer(string message, string error)
        {
            int sent = ServerSocket.Send(toBytes(message));
            Console.WriteLine("Sending: " + message);
            if (sent == message.Length)
                return message;
            return "ERROR - " + error;
        }
        static public byte[] toBytes(string str)
        {
            return Encoding.ASCII.GetBytes(str);
        }
        static public string toString(byte[] byt)
        {
            return byt.ToString();
        }
    
    }

}
