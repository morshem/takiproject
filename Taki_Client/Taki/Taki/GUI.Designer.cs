﻿namespace Taki
{
    partial class GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GUI));
            this.chat = new System.Windows.Forms.RichTextBox();
            this.writeChat = new System.Windows.Forms.RichTextBox();
            this.console = new System.Windows.Forms.RichTextBox();
            this.userName = new System.Windows.Forms.RichTextBox();
            this.password = new System.Windows.Forms.RichTextBox();
            this.login = new System.Windows.Forms.Button();
            this.register = new System.Windows.Forms.Button();
            this.logout = new System.Windows.Forms.Button();
            this.toLobby = new System.Windows.Forms.Button();
            this.userNameLabel = new System.Windows.Forms.Label();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.chatLabel = new System.Windows.Forms.Label();
            this.consoleLabel = new System.Windows.Forms.Label();
            this.player1 = new System.Windows.Forms.Label();
            this.cardCount1 = new System.Windows.Forms.Label();
            this.drawButton = new System.Windows.Forms.Button();
            this.endTurnButton = new System.Windows.Forms.Button();
            this.hand = new System.Windows.Forms.ComboBox();
            this.resetHandQueue = new System.Windows.Forms.Button();
            this.cardsQueueBox = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.topCard = new System.Windows.Forms.Label();
            this.chooseColorList = new System.Windows.Forms.ComboBox();
            this.roomListBox = new System.Windows.Forms.ComboBox();
            this.lobbyGif = new System.Windows.Forms.PictureBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.smokeWeed = new System.Windows.Forms.Label();
            this.createRoomButton = new System.Windows.Forms.Button();
            this.beginListening = new System.Windows.Forms.Button();
            this.getRoomList = new System.Windows.Forms.Button();
            this.startGameButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.lobbyGif)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // chat
            // 
            this.chat.BackColor = System.Drawing.SystemColors.ControlLight;
            this.chat.Location = new System.Drawing.Point(12, 30);
            this.chat.Name = "chat";
            this.chat.ReadOnly = true;
            this.chat.Size = new System.Drawing.Size(270, 390);
            this.chat.TabIndex = 0;
            this.chat.Text = "";
            // 
            // writeChat
            // 
            this.writeChat.Location = new System.Drawing.Point(12, 426);
            this.writeChat.Name = "writeChat";
            this.writeChat.Size = new System.Drawing.Size(269, 34);
            this.writeChat.TabIndex = 1;
            this.writeChat.Text = "";
            this.writeChat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.writeChat_KeyPress);
            // 
            // console
            // 
            this.console.BackColor = System.Drawing.SystemColors.ControlText;
            this.console.EnableAutoDragDrop = true;
            this.console.ForeColor = System.Drawing.Color.Lime;
            this.console.Location = new System.Drawing.Point(288, 30);
            this.console.Name = "console";
            this.console.ReadOnly = true;
            this.console.Size = new System.Drawing.Size(300, 186);
            this.console.TabIndex = 3;
            this.console.Text = "";
            // 
            // userName
            // 
            this.userName.Location = new System.Drawing.Point(290, 398);
            this.userName.Name = "userName";
            this.userName.Size = new System.Drawing.Size(147, 29);
            this.userName.TabIndex = 4;
            this.userName.Text = "";
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(443, 398);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(147, 29);
            this.password.TabIndex = 4;
            this.password.Text = "";
            // 
            // login
            // 
            this.login.Location = new System.Drawing.Point(290, 433);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(67, 28);
            this.login.TabIndex = 5;
            this.login.Text = "Login";
            this.login.UseVisualStyleBackColor = true;
            this.login.Click += new System.EventHandler(this.login_Click);
            // 
            // register
            // 
            this.register.Location = new System.Drawing.Point(363, 433);
            this.register.Name = "register";
            this.register.Size = new System.Drawing.Size(73, 28);
            this.register.TabIndex = 5;
            this.register.Text = "Register";
            this.register.UseVisualStyleBackColor = true;
            this.register.Click += new System.EventHandler(this.register_Click);
            // 
            // logout
            // 
            this.logout.Location = new System.Drawing.Point(442, 432);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(73, 28);
            this.logout.TabIndex = 5;
            this.logout.Text = "Logout";
            this.logout.UseVisualStyleBackColor = true;
            this.logout.Click += new System.EventHandler(this.logout_Click);
            // 
            // toLobby
            // 
            this.toLobby.Location = new System.Drawing.Point(522, 433);
            this.toLobby.Name = "toLobby";
            this.toLobby.Size = new System.Drawing.Size(68, 28);
            this.toLobby.TabIndex = 5;
            this.toLobby.Text = "To Lobby";
            this.toLobby.UseVisualStyleBackColor = true;
            this.toLobby.Click += new System.EventHandler(this.toLobby_Click);
            // 
            // userNameLabel
            // 
            this.userNameLabel.AutoSize = true;
            this.userNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userNameLabel.Location = new System.Drawing.Point(287, 382);
            this.userNameLabel.Name = "userNameLabel";
            this.userNameLabel.Size = new System.Drawing.Size(65, 15);
            this.userNameLabel.TabIndex = 6;
            this.userNameLabel.Text = "Username";
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.passwordLabel.Location = new System.Drawing.Point(443, 382);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(61, 15);
            this.passwordLabel.TabIndex = 6;
            this.passwordLabel.Text = "Password";
            // 
            // chatLabel
            // 
            this.chatLabel.AutoSize = true;
            this.chatLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.chatLabel.Location = new System.Drawing.Point(13, 11);
            this.chatLabel.Name = "chatLabel";
            this.chatLabel.Size = new System.Drawing.Size(39, 16);
            this.chatLabel.TabIndex = 8;
            this.chatLabel.Text = "Chat";
            // 
            // consoleLabel
            // 
            this.consoleLabel.AutoSize = true;
            this.consoleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.consoleLabel.Location = new System.Drawing.Point(288, 11);
            this.consoleLabel.Name = "consoleLabel";
            this.consoleLabel.Size = new System.Drawing.Size(65, 16);
            this.consoleLabel.TabIndex = 8;
            this.consoleLabel.Text = "Console";
            // 
            // player1
            // 
            this.player1.AutoSize = true;
            this.player1.Location = new System.Drawing.Point(289, 245);
            this.player1.Name = "player1";
            this.player1.Size = new System.Drawing.Size(27, 13);
            this.player1.TabIndex = 9;
            this.player1.Text = "N/A";
            // 
            // cardCount1
            // 
            this.cardCount1.AutoSize = true;
            this.cardCount1.Location = new System.Drawing.Point(330, 245);
            this.cardCount1.Name = "cardCount1";
            this.cardCount1.Size = new System.Drawing.Size(27, 13);
            this.cardCount1.TabIndex = 10;
            this.cardCount1.Text = "N/A";
            // 
            // drawButton
            // 
            this.drawButton.Location = new System.Drawing.Point(455, 245);
            this.drawButton.Name = "drawButton";
            this.drawButton.Size = new System.Drawing.Size(133, 35);
            this.drawButton.TabIndex = 11;
            this.drawButton.Text = "Draw Card(s)";
            this.drawButton.UseVisualStyleBackColor = true;
            this.drawButton.Click += new System.EventHandler(this.drawButton_Click);
            // 
            // endTurnButton
            // 
            this.endTurnButton.Location = new System.Drawing.Point(455, 289);
            this.endTurnButton.Name = "endTurnButton";
            this.endTurnButton.Size = new System.Drawing.Size(131, 35);
            this.endTurnButton.TabIndex = 12;
            this.endTurnButton.Text = "End Turn";
            this.endTurnButton.UseVisualStyleBackColor = true;
            this.endTurnButton.Click += new System.EventHandler(this.endTurnButton_Click);
            // 
            // hand
            // 
            this.hand.Cursor = System.Windows.Forms.Cursors.Default;
            this.hand.FormattingEnabled = true;
            this.hand.Location = new System.Drawing.Point(288, 218);
            this.hand.Name = "hand";
            this.hand.Size = new System.Drawing.Size(147, 21);
            this.hand.TabIndex = 13;
            this.hand.SelectionChangeCommitted += new System.EventHandler(this.hand_SelectionChangeCommitted);
            // 
            // resetHandQueue
            // 
            this.resetHandQueue.Location = new System.Drawing.Point(520, 218);
            this.resetHandQueue.Name = "resetHandQueue";
            this.resetHandQueue.Size = new System.Drawing.Size(66, 21);
            this.resetHandQueue.TabIndex = 14;
            this.resetHandQueue.Text = "Reset";
            this.resetHandQueue.UseVisualStyleBackColor = true;
            this.resetHandQueue.Click += new System.EventHandler(this.resetHandButton_Click);
            // 
            // cardsQueueBox
            // 
            this.cardsQueueBox.FormattingEnabled = true;
            this.cardsQueueBox.Location = new System.Drawing.Point(455, 218);
            this.cardsQueueBox.Name = "cardsQueueBox";
            this.cardsQueueBox.Size = new System.Drawing.Size(63, 21);
            this.cardsQueueBox.TabIndex = 15;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(591, 433);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(10, 12);
            this.button1.TabIndex = 16;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // topCard
            // 
            this.topCard.AutoSize = true;
            this.topCard.Font = new System.Drawing.Font("Lucida Sans", 56.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.topCard.Location = new System.Drawing.Point(363, 244);
            this.topCard.Name = "topCard";
            this.topCard.Size = new System.Drawing.Size(86, 85);
            this.topCard.TabIndex = 17;
            this.topCard.Text = "^";
            // 
            // chooseColorList
            // 
            this.chooseColorList.FormattingEnabled = true;
            this.chooseColorList.Location = new System.Drawing.Point(361, 332);
            this.chooseColorList.Name = "chooseColorList";
            this.chooseColorList.Size = new System.Drawing.Size(73, 21);
            this.chooseColorList.TabIndex = 18;
            this.chooseColorList.Visible = false;
            this.chooseColorList.SelectionChangeCommitted += new System.EventHandler(this.chooseColorList_SelectionChangeCommitted);
            // 
            // roomListBox
            // 
            this.roomListBox.FormattingEnabled = true;
            this.roomListBox.Location = new System.Drawing.Point(288, 358);
            this.roomListBox.Name = "roomListBox";
            this.roomListBox.Size = new System.Drawing.Size(302, 21);
            this.roomListBox.TabIndex = 19;
            this.roomListBox.SelectionChangeCommitted += new System.EventHandler(this.roomListBox_SelectionChangeCommitted);
            // 
            // lobbyGif
            // 
            this.lobbyGif.BackColor = System.Drawing.Color.White;
            this.lobbyGif.Cursor = System.Windows.Forms.Cursors.No;
            this.lobbyGif.Image = ((System.Drawing.Image)(resources.GetObject("lobbyGif.Image")));
            this.lobbyGif.Location = new System.Drawing.Point(287, 219);
            this.lobbyGif.Name = "lobbyGif";
            this.lobbyGif.Size = new System.Drawing.Size(157, 139);
            this.lobbyGif.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.lobbyGif.TabIndex = 20;
            this.lobbyGif.TabStop = false;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // smokeWeed
            // 
            this.smokeWeed.AutoSize = true;
            this.smokeWeed.Font = new System.Drawing.Font("Adobe Gothic Std B", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.smokeWeed.Location = new System.Drawing.Point(457, 219);
            this.smokeWeed.Name = "smokeWeed";
            this.smokeWeed.Size = new System.Drawing.Size(81, 104);
            this.smokeWeed.TabIndex = 21;
            this.smokeWeed.Text = "SMOKE\r\nWEED\r\nEVERY\r\nDAY";
            // 
            // createRoomButton
            // 
            this.createRoomButton.Enabled = false;
            this.createRoomButton.Location = new System.Drawing.Point(537, 221);
            this.createRoomButton.Name = "createRoomButton";
            this.createRoomButton.Size = new System.Drawing.Size(50, 107);
            this.createRoomButton.TabIndex = 22;
            this.createRoomButton.Text = "Create\r\nRoom";
            this.createRoomButton.UseVisualStyleBackColor = true;
            this.createRoomButton.Visible = false;
            this.createRoomButton.Click += new System.EventHandler(this.createRoomButton_Click);
            // 
            // beginListening
            // 
            this.beginListening.Location = new System.Drawing.Point(450, 330);
            this.beginListening.Name = "beginListening";
            this.beginListening.Size = new System.Drawing.Size(135, 22);
            this.beginListening.TabIndex = 23;
            this.beginListening.Text = "Begin Listening";
            this.beginListening.UseVisualStyleBackColor = true;
            this.beginListening.Click += new System.EventHandler(this.beginListening_Click);
            // 
            // getRoomList
            // 
            this.getRoomList.Enabled = false;
            this.getRoomList.Location = new System.Drawing.Point(537, 322);
            this.getRoomList.Name = "getRoomList";
            this.getRoomList.Size = new System.Drawing.Size(51, 36);
            this.getRoomList.TabIndex = 24;
            this.getRoomList.Text = "Sync \r\nRooms";
            this.getRoomList.UseVisualStyleBackColor = true;
            this.getRoomList.Visible = false;
            this.getRoomList.Click += new System.EventHandler(this.getRoomList_Click);
            // 
            // startGameButton
            // 
            this.startGameButton.Enabled = false;
            this.startGameButton.Location = new System.Drawing.Point(452, 326);
            this.startGameButton.Name = "startGameButton";
            this.startGameButton.Size = new System.Drawing.Size(85, 30);
            this.startGameButton.TabIndex = 25;
            this.startGameButton.Text = "Start Game";
            this.startGameButton.UseVisualStyleBackColor = true;
            this.startGameButton.Visible = false;
            this.startGameButton.Click += new System.EventHandler(this.startGameButton_Click);
            // 
            // GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(604, 461);
            this.Controls.Add(this.startGameButton);
            this.Controls.Add(this.getRoomList);
            this.Controls.Add(this.beginListening);
            this.Controls.Add(this.createRoomButton);
            this.Controls.Add(this.smokeWeed);
            this.Controls.Add(this.lobbyGif);
            this.Controls.Add(this.roomListBox);
            this.Controls.Add(this.chooseColorList);
            this.Controls.Add(this.topCard);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cardsQueueBox);
            this.Controls.Add(this.resetHandQueue);
            this.Controls.Add(this.hand);
            this.Controls.Add(this.endTurnButton);
            this.Controls.Add(this.drawButton);
            this.Controls.Add(this.cardCount1);
            this.Controls.Add(this.player1);
            this.Controls.Add(this.consoleLabel);
            this.Controls.Add(this.chatLabel);
            this.Controls.Add(this.passwordLabel);
            this.Controls.Add(this.userNameLabel);
            this.Controls.Add(this.logout);
            this.Controls.Add(this.register);
            this.Controls.Add(this.toLobby);
            this.Controls.Add(this.login);
            this.Controls.Add(this.password);
            this.Controls.Add(this.userName);
            this.Controls.Add(this.console);
            this.Controls.Add(this.writeChat);
            this.Controls.Add(this.chat);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "GUI";
            this.Text = "Taki!";
            ((System.ComponentModel.ISupportInitialize)(this.lobbyGif)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox chat;
        private System.Windows.Forms.RichTextBox writeChat;
        private System.Windows.Forms.RichTextBox console;
        private System.Windows.Forms.RichTextBox userName;
        private System.Windows.Forms.RichTextBox password;
        private System.Windows.Forms.Button login;
        private System.Windows.Forms.Button register;
        private System.Windows.Forms.Button logout;
        private System.Windows.Forms.Button toLobby;
        private System.Windows.Forms.Label userNameLabel;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.Label chatLabel;
        private System.Windows.Forms.Label consoleLabel;
        private System.Windows.Forms.Label player1;
        private System.Windows.Forms.Label cardCount1;
        private System.Windows.Forms.Button drawButton;
        private System.Windows.Forms.Button endTurnButton;
        private System.Windows.Forms.ComboBox hand;
        private System.Windows.Forms.Button resetHandQueue;
        private System.Windows.Forms.ComboBox cardsQueueBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label topCard;
        private System.Windows.Forms.ComboBox chooseColorList;
        private System.Windows.Forms.ComboBox roomListBox;
        private System.Windows.Forms.PictureBox lobbyGif;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label smokeWeed;
        private System.Windows.Forms.Button createRoomButton;
        private System.Windows.Forms.Button beginListening;
        private System.Windows.Forms.Button getRoomList;
        private System.Windows.Forms.Button startGameButton;
    }
}